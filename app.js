const electron = require('electron');
const { ipcRenderer } = electron;

document.querySelector('#video-select').addEventListener('submit', (event) => {
  event.preventDefault();
  let form = event.target;
  let video = form.querySelector('input').files[0];
  if (empty(video)) {
    alert('Please select a video file');
  }
  const { path } = video;
  ipcRenderer.send('video:selected', path);
  
});

ipcRenderer.on('video:duration', (event, duration) => {
  document.querySelector('#video-duration').innerText = `Video duration is: ${duration} (seconds)`;
});

function empty(element) {
  if (element == null || element == '') {
    return true;
  }
  return false;
}

const electron = require('electron');
const ffmpeg = require('fluent-ffmpeg');

const { app, BrowserWindow, ipcMain } = electron;

let mainWindow;
app.on('ready', () => {
  mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  mainWindow.loadURL(`file://${__dirname}/index.html`);
  console.log('I am ready to serve you');
});

ipcMain.on('video:selected', (event, path) => {
  ffmpeg.ffprobe(path, (err, metadata) => {
    console.log('Video Selected has a duration of: ', metadata.format.duration);
    mainWindow.webContents.send('video:duration', metadata.format.duration);
  });
});
